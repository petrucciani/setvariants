#!/bin/bash

ocamlbuild -use-menhir -use-ocamlfind \
	-cflags '-I '`pwd`/../cduce/lib/ \
	-lflags '-I '`pwd`'/../cduce/lib/ 'cduce_lib.cmxa main.native
