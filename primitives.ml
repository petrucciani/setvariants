(* © 2016-2022 Tommaso Petrucciani, see the README.md and LICENSE.md files *)
open Utils
open Syntax

module CD = Cduce_types

let any = CD.Types.any
let arrow = CD.Types.arrow
let times = CD.Types.times
let cons = CD.Types.cons
let cup = CD.Types.cup
let diff = CD.Types.diff
let mk_atom s = s |> CD.AtomSet.V.mk_ascii |> CD.AtomSet.atom |> CD.Types.atom

let fail err = raise (Primitive_error err)

let int_op op op_name v1 v2 =
  match v1, v2 with
  | `Const (CInt x1), `Const (CInt x2) -> `Const (CInt (op x1 x2))
  | _ -> fail (op_name ^ ": requires integer arguments")

let primitives =
  let t_bool = CD.Types.cup (mk_atom "true") (mk_atom "false") in
  let t_int = CD.Types.interval (CD.Intervals.bounded
    (CD.Intervals.V.from_int min_int) (CD.Intervals.V.from_int max_int))
  in
  let zero = 0 |> CD.Intervals.V.from_int |> CD.Intervals.atom
    |> CD.Types.interval
  in
  [
    ("eq",
     (let rec eq v1 v2 =
        match v1, v2 with
        | `Const c1,           `Const c2           -> c1 = c2
        | `Pair (v11, v12),    `Pair (v21, v22)    -> eq v11 v21 && eq v12 v22
        | `Variant (tag1, v1), `Variant (tag2, v2) -> tag1 = tag2 && eq v1 v2
        | `Closure _,          _
        | `Primitive _,        _
        | _,                   `Closure _
        | _,                   `Primitive _        ->
            fail "eq: comparing functional values"
        | _                                        -> false
      in
      fun v1 ->
        `Primitive ("eq:partially_applied",
          fun v2 -> `Const (CBool (eq v1 v2)))),
     (Types.mono_scheme
        (arrow (cons any) (cons (arrow (cons any) (cons t_bool)))))
   );
  ("succ",
   (function
    | `Const (CInt n) -> `Const (CInt (n + 1))
    | _ -> fail "succ: requires integer argument"),
   (Types.mono_scheme (arrow (cons t_int) (cons t_int))));
  ("add",
   (fun v1 ->
      `Primitive("add:partially_applied", fun v2 -> int_op ( + ) "add" v1 v2)),
   (Types.mono_scheme
      (arrow (cons t_int) (cons (arrow (cons t_int) (cons t_int))))));
  ("sub",
   (fun v1 ->
      `Primitive("sub:partially_applied", fun v2 -> int_op ( - ) "sub" v1 v2)),
   (Types.mono_scheme
      (arrow (cons t_int) (cons (arrow (cons t_int) (cons t_int))))));
  ("mul",
   (fun v1 ->
      `Primitive("mul:partially_applied", fun v2 -> int_op ( * ) "mul" v1 v2)),
   (Types.mono_scheme
      (arrow (cons t_int) (cons (arrow (cons t_int) (cons t_int))))));
  ("div",
   (fun v1 ->
      `Primitive("div:partially_applied", fun v2 ->
        match v1, v2 with
        | `Const (CInt x1), `Const (CInt x2) when x2 <> 0 ->
            `Const (CInt (x1 / x2))
        | _, `Const (CInt 0) -> fail "div: division by zero"
        | _ -> fail "div: requires integer arguments")),
   (Types.mono_scheme
      (arrow (cons t_int) (cons (arrow (cons t_int) (cons t_int))))));
  ("safe_div",
   (fun v1 ->
      `Primitive("safe_div:partially_applied", fun v2 ->
        match v1, v2 with
        | `Const (CInt x1), `Const (CInt x2) when x2 <> 0 ->
            `Const (CInt (x1 / x2))
        | _ -> fail "safe_div: requires integer arguments")),
   (Types.mono_scheme
      (arrow (cons t_int)
        (cons (arrow (cons (diff t_int zero)) (cons t_int))))))
  ]

let naming =
  List.map (fun (x, _, _) -> x) primitives |> Env.of_list

let values =
  List.map (fun (x, op, _) -> `Primitive (x, op)) primitives
  |> Env.of_list

let typing =
  let open Types.Type_env in
  List.fold_left (fun acc (x, _, s) -> add x s acc) empty primitives


let register_globals () =
  let open CD in
  Format.eprintf "here\n%!";
  let alpha = Var.mk "a" in
  let t_alpha = Types.var alpha in
  let alphalist = Types.make () in
  Types.define alphalist
    (Types.cup
      (CD.Types.xml
        ("Cons" |> mk_atom |> CD.Types.cons)
        (CD.Types.times
          (CD.Types.empty_closed_record |> CD.Types.cons)
          (Types.times (cons t_alpha) alphalist |> CD.Types.cons)
        |> CD.Types.cons))
      (mk_atom "Nil"));
  Types.Print.register_global
  ""  (CD.Ident.mk "List") ~params:[t_alpha] (Types.descr alphalist)