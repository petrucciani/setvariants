(* © 2016-2022 Tommaso Petrucciani, see the README.md and LICENSE.md files *)
open Utils

(* Language constants. *)
type constant =
	| CUnit
	| CBool of bool
	| CInt of int

(* Patterns. *)
type pattern =
	| PWcard
	| PVar of name
	| PConst of constant
	| PFun
	| PPair of pattern * pattern
	| PVariant of label * pattern
	| PVariant' of label
	| PAnd of pattern * pattern
	| POr of pattern * pattern

(* Expressions (internal syntax).
   In `Abstr(Some f, x, e), f is the recursion parameter. *)
type 'var_type int_expr =
	[ `Var of 'var_type
	| `Const of constant
	| `Abstr of name option * name * 'var_type int_expr
	| `Appl of 'var_type int_expr * 'var_type int_expr
	| `Pair of 'var_type int_expr * 'var_type int_expr
	| `Variant of label * 'var_type int_expr
	| `Variant' of label
	| `Match of 'var_type int_expr * (pattern * 'var_type int_expr) list
	]

(* Returns the set of capture variables of a pattern. *)
val capt: pattern -> StringSet.t
val how_many_capt: pattern -> int
(* Returns the capture variables of a pattern as a naming environment. *)
val pattern_naming: pattern -> naming

(* Expressions with variable names. *)
type expr = name int_expr

(* Expressions in De Bruijn notation. *)
type db_expr = int int_expr

(* Primitive operations can raise Primitive_error for runtime errors. *)
exception Primitive_error of string

type primitive_op = value -> value

(* Values. *)
and value =
	[ `Const of constant
	| `Closure of name option * name * db_expr * closure_env
	| `Pair of value * value
	| `Variant of label * value
	| `Variant' of label
	| `Primitive of name * primitive_op
	]

(* An environment which records values for closures. *)
and closure_env = value Env.t

(* Extended syntax for expressions, with syntactic sugar.
   In `Let_abstr, the boolean is true if the function is recursive. *)
type ext_expr =
	[ `Var of name
	| `Const of constant
	| `Abstr of name option * pattern list * ext_expr
	| `Abstr_cases of name option * (pattern * ext_expr) list
	| `Appl of ext_expr * ext_expr
	| `Pair of ext_expr * ext_expr
	| `Variant of label * ext_expr
	| `Variant' of label
	| `Match of ext_expr * (pattern * ext_expr) list
	| `Let of pattern * ext_expr * ext_expr
	| `If_then_else of ext_expr * ext_expr * ext_expr
	| `Empty_list
	| `Cons of ext_expr * ext_expr
	| `List of  ext_expr list
	| `Let_abstr of name * bool * pattern list * ext_expr * ext_expr
	]

(* Converts external into internal syntax, eliminating syntactic sugar. *)
val desugar: ext_expr -> expr

(* Converts expressions to use De Bruijn notation. *)
val de_bruijn: naming -> expr -> db_expr

(* Top-level statements. *)
type statement =
	| Binding of pattern * ext_expr
	| Expr of ext_expr
	| Quit
