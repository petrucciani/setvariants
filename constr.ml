(* © 2016-2022 Tommaso Petrucciani, see the README.md and LICENSE.md files *)
open Utils
open Syntax

module CD = Cduce_types
module Tenv = Types.Type_env

let do_print_tallying_constraints = ref false

(* Constraint representation. *)

type constr =
  | CSub of Types.t * Types.t
  | CInst of name * Types.t
  | CDef of Types.mono_env * constrset
  | CLet of constrset * ((Types.mono_env * constr list) list)

and constrset = constr list

type typeconstr = Types.t * Types.t

(* Constraint generation. *)

type typ =
  [ `Var of Types.var
  | `Const of Syntax.constant
  | `Arrow of typ * typ
  | `Product of typ * typ
  | `Variant of label * typ
  | `Variant' of label
  | `Or of typ * typ
  | `And of typ * typ
  | `Diff of typ * typ
  | `Empty
  | `Any
  | `Cduce of Types.t
  ]

type constr' =
  [ `Sub of typ * typ
  | `Inst of name * typ
  | `Def of typ Tenv.t * constr' list
  | `Let of constr' list * (typ Tenv.t * constr' list) list
  ]

let fresh () = `Var (Types.fresh_var ())
let ( <: ) t t' = `Sub (t, t')
let ( <:: ) x t = `Inst (x, t)

let rec acc = function
  | PWcard -> `Any
  | PVar _ -> `Any
  | PConst c -> `Const c
  | PFun -> `Arrow (`Empty, `Any)
  | PPair (p1, p2) -> `Product (acc p1, acc p2)
  | PVariant (tag, p) -> `Variant (tag, acc p)
  | PVariant' tag -> `Variant' tag
  | PAnd (p1, p2) -> `And (acc p1, acc p2)
  | POr (p1, p2) -> `Or (acc p1, acc p2)

let rec pattern_gen t =
  function
  | PWcard -> (Tenv.empty, [])
  | PVar x -> (Tenv.singleton x t, [])
  | PConst _ -> (Tenv.empty, [])
  | PFun -> (Tenv.empty, [])
  | PPair (p1, p2) ->
      let alpha1, alpha2 = fresh (), fresh () in
      let alpha1, (env1, c1) = pattern_simpl alpha1 (pattern_gen alpha1 p1) in
      let alpha2, (env2, c2) = pattern_simpl alpha2 (pattern_gen alpha2 p2) in
      if alpha1 = `Any && alpha2 = `Any then (Tenv.empty, [])
      else (Tenv.update env1 env2, (t <: `Product (alpha1, alpha2)) :: c1 @ c2)
  | PVariant (tag, p) ->
      let alpha = fresh () in
      let alpha, (env, c) = pattern_simpl alpha (pattern_gen alpha p) in
      if alpha = `Any then (Tenv.empty, [])
      else (env, (t <: `Variant (tag, alpha)) :: c)
  | PVariant' _ ->
      (Tenv.empty, [])
  | PAnd (p1, p2) ->
      let (env1, c1) = pattern_gen t p1 in
      let (env2, c2) = pattern_gen t p2 in
      (Tenv.update env1 env2, c1 @ c2)
  | POr (p1, p2) ->
      let (env1, c1) = pattern_gen (`And (t, acc p1)) p1 in
      let (env2, c2) = pattern_gen (`Diff (t, acc p1)) p2 in
      let env =
        Tenv.merge (fun x t1 t2 ->
            match t1, t2 with
            | Some t1', Some t2' -> Some (`Or (t1', t2'))
            | _ -> None)
          env1 env2
      in
      (env, c1 @ c2)

and pattern_simpl alpha (env, c) =
  if Tenv.is_empty env then (`Any, (env, []))
  else (alpha, (env, c))

(* tdisj [t1 ... tn] = t1 | ... | tn *)
let rec tdisj = function
  | t :: ts -> List.fold_left (fun t t' -> `Or (t, t')) t ts
  | [] -> `Empty

(* exhaustiveness t [pi -> ei | i = 1,...,n] = t <: acc(p1) | ... | acc(pn) *)
let exhaustiveness t clauses =
   t <: tdisj (List.map (fun (pi, _) -> acc pi) clauses)

(* branch_pattern_types [pi -> ei | i = 1,...,n] = [t1 ... tn]
   where ti = acc(pi) \ Union_{j < i} acc(pj) *)
let branch_pattern_types clauses =
  let rec aux t = function
    | (p, e) :: cs -> let t' = acc p in (`Diff (t', t)) :: aux (`Or (t, t')) cs
    | [] -> []
  in
  match clauses with
  | (p, e) :: clauses' -> let t = acc p in t :: aux t clauses'
  | [] -> assert false

(* is_surely_monomorphic lambda_vars e0 [pi -> ei | i]:
     true if the patterns have no capture variables
     or if e0 is surely monomorphic (constructed out of variables in
     env - which are monomorphic -, constants, pairs and variants). *)
let is_surely_monomorphic =
  let rec surely_monomorphic_expr env = function
    | `Var x -> Tenv.mem x env
    | `Const _ -> true
    | `Pair (e1, e2) -> surely_monomorphic_expr env e1 &&
        surely_monomorphic_expr env e2
    | `Variant (_, e) -> surely_monomorphic_expr env e
    | `Variant' _ -> true
    | _ -> false
  in
  fun env e0 clauses ->
    surely_monomorphic_expr env e0 ||
    List.for_all (fun (pi, _) -> how_many_capt pi = 0) clauses

(* Builds the environment for a function:
   (x: t), plus (f: t -> t') if f is recursive. *)
let function_env ~f ~x ~dom ~rng =
  (match f with
  | Some f' -> Tenv.singleton f' (`Arrow (dom, rng))
  | None -> Tenv.empty)
  |> Tenv.add x dom

(* branches_and_cis ~ispoly env t t' [pi -> ei | i] =
    ([(envi, ci') | i], Union_i ci)
   where:
   - t is the type of the matched expression, y' that of the result
   - (envi, ci) are the environment and constraints generated for pi
   - ci' is the constraint generated for ei
   - ~ispoly:true when the match construct can introduce polymorphism
   - env is the environment of monomorphic variables. *)
let rec branches_and_cis ~ispoly env t t' clauses =
  let bcis =
    List.map2 (fun (pi, ei) pti ->
        let ti = `And (t, pti) in
        let (envi, ci) = pattern_gen ti pi in
        let env' =
          if ispoly then Tenv.filter (fun x _ -> not (Tenv.mem x envi)) env
          else Tenv.update env envi
        in
        let ci' = constrgen env' t' ei in
        ((envi, ci'), ci)
      )
      clauses (branch_pattern_types clauses)
  in
  (List.map fst bcis, List.concat (List.map snd bcis))

and simpl alpha cs =
  match cs with
  | `Simpl (`Sub (t, alpha'), cs) when alpha = alpha' -> (t, cs)
  | `Simpl (c, cs) -> (alpha, c :: cs)
  | `Compl cs -> (alpha, cs)

(* Note that env only contains monomorphic variables: polymorphic ones
   are directly recorded in the constraints. *)
and constrgen' env t = function
  | `Var x ->
      (match Tenv.find_opt x env with Some t' -> `Simpl (t' <: t, []) | None -> `Compl [x <:: t])
  | `Const c -> `Simpl (`Const c <: t, [])
  | `Abstr (f, x, `Match (`Var x', clauses)) when x = x' ->
      assert (clauses <> []);
      let alpha, beta, usedfreshvars = match t with
        | `Arrow (t1, t2) -> t1, t2, false
        | _ -> fresh (), fresh (), true
      in
      let env_f = function_env ~f ~x ~dom:alpha ~rng:beta in
      let env' = Tenv.update env env_f in
      let (branches, cis) =
        branches_and_cis ~ispoly:false env' alpha beta clauses
      in
      let c = (exhaustiveness alpha clauses) :: cis @
        List.map (fun c -> `Def c) branches
      in
      if usedfreshvars
      then `Simpl (`Arrow (alpha, beta) <: t, [`Def (env_f, c)])
      else `Compl [`Def (env_f, c)]
  | `Abstr (f, x, e) ->
      let alpha, beta, usedfreshvars = match t with
        | `Arrow (t1, t2) -> t1, t2, false
        | _ -> fresh (), fresh (), true
      in
      let env_f = function_env ~f ~x ~dom:alpha ~rng:beta in
      let env' = Tenv.update env env_f in
      let c = constrgen env' beta e
      in
      if usedfreshvars
      then `Simpl (`Arrow (alpha, beta) <: t, [`Def (env_f, c)])
      else `Compl [`Def (env_f, c)]
  | `Appl (e1, e2) ->
      let alpha = fresh () in
      let alpha, c2 = simpl alpha (constrgen' env alpha e2) in
      let c1 = constrgen env (`Arrow (alpha, t)) e1 in
      `Compl (c1 @ c2)
  | `Pair (e1, e2) ->
      let alpha1, alpha2 = fresh (), fresh () in
      let alpha1, c1 = simpl alpha1 (constrgen' env alpha1 e1) in
      let alpha2, c2 = simpl alpha2 (constrgen' env alpha2 e2) in
      `Simpl (`Product (alpha1, alpha2) <: t, c1 @ c2)
  | `Variant (tag, e) ->
      let alpha = fresh () in
      let alpha, c = simpl alpha (constrgen' env alpha e) in
      `Simpl (`Variant (tag, alpha) <: t, c)
  | `Variant' tag ->
      `Simpl (`Variant' tag <: t, [])
  | `Match (e0, clauses) when is_surely_monomorphic env e0 clauses ->
      assert (clauses <> []);
      let alpha = fresh () in
      let c0 = constrgen env alpha e0 in
      let (branches, cis) =
        branches_and_cis ~ispoly:false env alpha t clauses
      in
      `Compl (exhaustiveness alpha clauses :: c0 @ cis @
        (List.map (fun c -> `Def c) branches))
  | `Match (e0, clauses) ->
      assert (clauses <> []);
      let alpha = fresh () in
      let c0 = constrgen env alpha e0 in
      let (branches, cis) =
        branches_and_cis ~ispoly:true env alpha t clauses
      in
      let c0' = exhaustiveness alpha clauses :: c0 @ cis in
      `Compl [`Let (c0', branches)]

and constrgen env t e =
  match constrgen' env t e with
  | `Simpl (c, cs) -> c :: cs
  | `Compl cs -> cs

let mk_atom s = s |> CD.AtomSet.V.mk_ascii |> CD.AtomSet.atom |> CD.Types.atom

let rec convert_type: typ -> Types.t =
  function
  | `Var v -> CD.Types.var v
  | `Const c ->
      begin match c with
        | CUnit -> mk_atom "unit"
        | CBool true -> mk_atom "true"
        | CBool false -> mk_atom "false"
        | CInt n ->
            n |> CD.Intervals.V.from_int |> CD.Intervals.atom
              |> CD.Types.interval
      end
  | `Arrow (t1, t2) ->
      CD.Types.arrow
        (t1 |> convert_type |> CD.Types.cons)
        (t2 |> convert_type |> CD.Types.cons)
  | `Product (t1, t2) ->
      CD.Types.times
        (t1 |> convert_type |> CD.Types.cons)
        (t2 |> convert_type |> CD.Types.cons)
  | `Variant (tag, t) ->
      CD.Types.xml
       (tag |> mk_atom |> CD.Types.cons)
       (CD.Types.times
         (CD.Types.empty_closed_record |> CD.Types.cons)
         (t |> convert_type |> CD.Types.cons)
       |> CD.Types.cons)
  | `Variant' tag -> mk_atom tag
  | `Or (t1, t2) -> CD.Types.cup (convert_type t1) (convert_type t2)
  | `And (t1, t2) -> CD.Types.cap (convert_type t1) (convert_type t2)
  | `Diff (t1, t2) -> CD.Types.diff (convert_type t1) (convert_type t2)
  | `Empty -> CD.Types.empty
  | `Any -> CD.Types.any
  | `Cduce t -> t

let rec convert_constr: constr' -> constr =
  function
  | `Sub (t, t') -> CSub (convert_type t, convert_type t')
  | `Inst (x, t) -> CInst (x, convert_type t)
  | `Def (env, cs) ->
      CDef (Tenv.map convert_type env, List.map convert_constr cs)
  | `Let (cs, branches) ->
      let branches' = List.map (fun (envi, csi) ->
          (Tenv.map convert_type envi, List.map convert_constr csi))
        branches
      in
      CLet (List.map convert_constr cs, branches')

let generate t e =
  List.map convert_constr (constrgen Tenv.empty (`Cduce t) e)

let generate_pattern t p =
  let (env, cs) = pattern_gen (`Cduce t) p in
  (Tenv.map convert_type env, List.map convert_constr cs)

(* Handling non-determinism. *)

module Nondet = struct
  type 'a t = 'a list

  let return x = [x]
  let bind xs f = List.concat (List.map f xs)
  let ( >>= ) = bind

  let rec flatten = function
    | xs :: xss ->
        xs >>= fun x ->
          List.map (fun xs' -> x :: xs') (flatten xss)
  | [] -> [[]]

  let of_list xs = xs
  let to_list xs = xs
end

type tallyarg = typeconstr list * Types.subst list

let nondet_union d1s d2s =
  Nondet.bind d1s (fun (d1, thetas1) ->
    List.map (fun (d2, thetas2) -> (d1 @ d2, thetas1 @ thetas2)) d2s)

let nondet_union_many = function
  | h :: t -> List.fold_left nondet_union h t
  | [] -> []

(* Constraint rewriting and solving. *)

let rec crewr: Types.type_env -> constr -> tallyarg Nondet.t =
  fun env -> function
    | CSub (t, t') -> Nondet.return ([(t, t')], [])
    | CInst (x, t) ->
        Nondet.return ([(Types.instance (Tenv.find x env), t)], [])
    | CDef (env', cs) ->
        let env'' = Tenv.update env (Tenv.map Types.mono_scheme env') in
        rewrite env'' cs
    | CLet (cs0, branches) ->
        let open Nondet in
        rewrite env cs0 >>= fun (d0, thetas0) ->
          tally d0 thetas0 >>= fun theta ->
            let delta = Types.apply_subst_env theta env |> Types.all_mvars
            in
            List.map (fun (envi, ci) ->
                let envi' = envi
                  |> Types.apply_subst_mono_env theta
                  |> Types.gen_delta_mono_env delta
                in
                let env' = Tenv.update env envi' in
                rewrite env' ci)
              branches
            |> Nondet.flatten >>= fun dsis ->
              List.fold_left (fun (d1, thetas1) (d2, thetas2) ->
                  (d1 @ d2, thetas1 @ thetas2))
                ([], [theta]) dsis
              |> return

and rewrite env cs = nondet_union_many (List.map (crewr env) cs)

and tally d thetas =
  let allvars = CD.(
    d
    |> List.map (fun (t, t') ->
        Var.Set.cup (Types.Subst.vars t) (Types.Subst.vars t'))
    |> List.fold_left Var.Set.cup Var.Set.empty)
  in
  let equiv theta =
    CD.(theta
    |> Var.Map.filter (fun v _t -> Var.Set.mem allvars v)
    |> Var.Map.mapi_to_list (fun v t -> (CD.Types.var v, t))
    |> List.map (fun (t, t') -> [(t, t'); (t', t)]))
    |> List.concat
  in
  let d' = d @ (thetas |> List.map equiv |> List.concat) in
  if !do_print_tallying_constraints then begin
    Format.eprintf "Callying tallying on constraints:@\n%!";
    List.iter (fun (t1, t2) ->
        Format.eprintf "%a <: %a@\n%!"
          CD.Types.Print.print t1 CD.Types.Print.print t2)
      d';
    Format.eprintf "--- end constraints.@\n%!"
  end;
  let sols = CD.Types.Tallying.tallying  CD.Var.Set.empty d' in
  (* Format.eprintf "found %d solutions\n\n%!" (List.length sols); *)
  (*List.iter (fun sol ->
      Format.eprintf "Found solution: BEGIN\n%a\nEND\n\n%!"
        CD.Types.Subst.print sol) sols;*) (*;
      List.iter (fun (t, t') ->
        (*Format.eprintf "checking\n%!";
        Format.eprintf "t = %a\n%!" CD.Types.Print.pp_type t;
        Format.eprintf "t' = %a\n%!" CD.Types.Print.pp_type t';*)
        let (t1, t1') = (Types.apply_subst sol t, Types.apply_subst sol t')
        in
        (*Format.eprintf "t theta = %a\n%!" CD.Types.Print.pp_type t1;
        Format.eprintf "t' theta = %a\n%!" CD.Types.Print.pp_type t1';*)
        (* if not (CD.Types.subtype t1 t1') then Format.eprintf "very bad\n\n%!") *)
        assert (CD.Types.subtype t1 t1'))
        (*else Format.eprintf "not bad\n\n%!"*)
        ds
    ) sols;*)
  (* Format.eprintf "check passed\n\n%!"; *)
  Nondet.of_list sols

let solve env cs =
  try Nondet.(
    rewrite env cs >>= fun (d, thetas) ->
      tally d thetas)
  with Not_found -> Format.printf "Unbound variable.@\n%!"; []

(* Pretty printing. *)

let fprintf = Format.fprintf

let ppt = CD.Types.Print.print

let rec pp_constr ppf = function
  | CSub (t, t') -> fprintf ppf "[%a <: %a]" ppt t ppt t'
  | CInst (x, t) -> fprintf ppf "[%s <:: %a]" x ppt t
  | CDef (env, cs) ->
      fprintf ppf "@[def %a in@;<1 2>%a@]" Types.pp_mono_env env pp_constrset cs
  | CLet (cs0, branches) ->
      fprintf ppf "@[let %a@ %a@]"
        pp_constrset cs0
        (Format.pp_print_list
          (fun ppf (envi, csi) ->
            fprintf ppf "@,| { %a } in %a"
              Types.pp_mono_env envi pp_constrset csi))
        branches

and pp_constrset: Format.formatter -> constrset -> unit =
  fun ppf cs ->
    fprintf ppf "{ @[<hv>@,";
    Format.pp_print_list ~pp_sep:(fun ppf () -> fprintf ppf ",@ ")
      pp_constr ppf cs;
    fprintf ppf " @]}"
