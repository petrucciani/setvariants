(* © 2016-2022 Tommaso Petrucciani, see the README.md and LICENSE.md files *)
open Utils
open Syntax

let fprintf = Format.fprintf

let pp_constant ppf = function
  | CUnit -> fprintf ppf "()"
  | CBool b -> fprintf ppf "%b" b
  | CInt n -> fprintf ppf "%d" n

let opar_if ppf pos n =
  if pos >= n then fprintf ppf "("

let cpar_if ppf pos n =
  if pos >= n then fprintf ppf ")"

let pp_pattern ppf p =
  let rec aux pos ppf = function
    | PWcard -> fprintf ppf "_"
    | PVar x -> fprintf ppf "%s" x
    | PConst c -> pp_constant ppf c
    | PFun -> fprintf ppf "fun"
    | PPair (p1, p2) -> fprintf ppf "(%a,%a)" (aux pos) p1 (aux pos) p2
    | PVariant' "Nil" -> fprintf ppf "[]"
    | PVariant ("Cons", PPair (p1, p2)) ->
        if pos >= 3 then fprintf ppf "(";
        fprintf ppf "%a :: %a" (aux 3) p1 (aux 2) p2;
        if pos >= 3 then fprintf ppf ")"
    | PVariant' tag -> fprintf ppf "`%s" tag
    | PVariant (tag, p) ->
        if pos >= 4 then fprintf ppf "(";
        fprintf ppf "`%s %a" tag (aux 4) p;
        if pos >= 4 then fprintf ppf ")"
    | PAnd (p1, p2) ->
        if pos >= 2 then fprintf ppf "(";
        fprintf ppf "%a & %a" (aux 1) p1 (aux 1) p2;
        if pos >= 2 then fprintf ppf ")"
    | POr (p1, p2) ->
        if pos >= 1 then fprintf ppf "(";
        fprintf ppf "%a | %a" (aux 0) p1 (aux 0) p2;
        if pos >= 1 then fprintf ppf ")"
  in
  aux 0 ppf p

let pp_int_expr pp_var ppf e =
  let rec aux pp_var pos ppf = function
    | `Var v -> pp_var ppf v
    | `Const c -> pp_constant ppf c
    | `Abstr (f, x, e) ->
        opar_if ppf pos 1;
        fprintf ppf "%s %s -> %a"
          (match f with Some f' -> "recfun " ^ f' | None -> "fun")
          x (aux pp_var 0) e;
        cpar_if ppf pos 1
    | `Appl (e1, e2) ->
        opar_if ppf pos 5;
        fprintf ppf "%a %a" (aux pp_var 4) e1 (aux pp_var 5) e2;
        cpar_if ppf pos 5
    | `Pair (e1, e2) ->
        fprintf ppf "(%a, %a)" (aux pp_var 0) e1 (aux pp_var 0) e2
    | `Variant' tag ->
        fprintf ppf "`%s" tag
    | `Variant (tag, e) ->
        opar_if ppf pos 5;
        fprintf ppf "`%s %a" tag (aux pp_var 5) e;
        cpar_if ppf pos 5
    | `Match (e0, clauses) ->
        opar_if ppf pos 1;
        fprintf ppf "match %a with %a"
          (aux pp_var 1) e0
          (Format.pp_print_list
            ~pp_sep:(fun ppf () -> fprintf ppf " | ")
            (fun ppf (pi, ei) ->
              fprintf ppf "%a -> %a" pp_pattern pi (aux pp_var 1) ei))
            clauses;
        cpar_if ppf pos 1
  in
  aux pp_var 0 ppf e

let pp_expr ppf e = pp_int_expr (fun ppf x -> fprintf ppf "%s" x) ppf e
let pp_db_expr ppf e = pp_int_expr (fun ppf n -> fprintf ppf "{%d}" n) ppf e

let pp_ext_expr ppf e =
  let rec aux pos ppf = function
  | `Var x -> fprintf ppf "%s" x
  | `Const c -> pp_constant ppf c
  | `Abstr (f, ps, e) ->
      opar_if ppf pos 1;
      fprintf ppf "%s %a -> %a"
        (match f with Some f' -> "recfun " ^ f' | None -> "fun")
        (Format.pp_print_list
          ~pp_sep:(fun ppf () -> fprintf ppf " ") pp_pattern)
        ps
        (aux 0) e;
      cpar_if ppf pos 1
  | `Abstr_cases (f, clauses) ->
      opar_if ppf pos 1;
      fprintf ppf "%s %a"
        (match f with Some f' -> "recfunction " ^ f' | None -> "function")
        (Format.pp_print_list
          ~pp_sep:(fun ppf () -> fprintf ppf " | ")
          (fun ppf (pi, ei) ->
            fprintf ppf "%a -> %a" pp_pattern pi (aux 1) ei))
        clauses;
      cpar_if ppf pos 1
  | `Appl (e1, e2) ->
      opar_if ppf pos 5;
      fprintf ppf "%a %a" (aux 4) e1 (aux 5) e2;
      cpar_if ppf pos 5
  | `Pair (e1, e2) ->
      fprintf ppf "(%a, %a)" (aux 0) e1 (aux 0) e2
  | `Variant' tag ->
      fprintf ppf "`%s" tag
  | `Variant (tag, e) ->
      opar_if ppf pos 5;
      fprintf ppf "`%s %a" tag (aux 5) e;
      cpar_if ppf pos 5
  | `Match (e0, clauses) ->
      opar_if ppf pos 1;
      fprintf ppf "match %a with %a"
        (aux 1) e0
        (Format.pp_print_list
          ~pp_sep:(fun ppf () -> fprintf ppf " | ")
          (fun ppf (pi, ei) ->
            fprintf ppf "%a -> %a" pp_pattern pi (aux 1) ei))
        clauses;
      cpar_if ppf pos 1
  | `Let (p, e1, e2) ->
      opar_if ppf pos 2;
      fprintf ppf "let %a = %a in %a"
        pp_pattern p (aux 0) e1 (aux 0) e2;
      cpar_if ppf pos 2
  | `If_then_else (e1, e2, e3) ->
      opar_if ppf pos 2;
      fprintf ppf "if %a then %a else %a"
        (aux 0) e1 (aux 0) e2 (aux 0) e3;
      cpar_if ppf pos 2
  | `Empty_list -> fprintf ppf "[]"
  | `Cons (e1, e2) ->
      opar_if ppf pos 3;
      fprintf ppf "%a :: %a" (aux 3) e1 (aux 2) e2;
      cpar_if ppf pos 3
  | `List es ->
      fprintf ppf "[%a]"
        (Format.pp_print_list
          ~pp_sep:(fun ppf () -> fprintf ppf "; ")
          (fun ppf e -> aux 0 ppf e))
        es
  | `Let_abstr (f, isrec, ps, e1, e2) ->
      opar_if ppf pos 2;
      fprintf ppf "%s %s %a = %a in %a"
        (if isrec then "letrec" else "let") f
        (Format.pp_print_list
          ~pp_sep:(fun ppf () -> fprintf ppf " ") pp_pattern)
        ps
        (aux 0) e1 (aux 0) e2;
      cpar_if ppf pos 2
  in
  aux 0 ppf e

let rec pp_fun ppf = function
  | `Closure (f, x, e, env) ->
      fprintf ppf "(%s %s -> %a"
        (match f with Some f' -> "recfun " ^ f' | None -> "fun") x
        pp_db_expr e;
      if not (Env.is_empty env) then fprintf ppf " where ";
      Env.iteri (fun i v ->
          fprintf ppf "{%d} = %a, " (i + 1) (pp_value_aux pp_fun) v)
        env;
      fprintf ppf ")"
  | `Primitive (x, _) -> fprintf ppf "<primitive:%s>" x

and pp_value_aux pp_fun ppf = function
  | `Const c -> pp_constant ppf c
  | `Pair (v1, v2) ->
      fprintf ppf "(%a, %a)" (pp_value_aux pp_fun) v1 (pp_value_aux pp_fun) v2
  | `Variant (tag, v) ->
      fprintf ppf "`%s %a" tag (pp_value_aux pp_fun) v
  | `Variant' tag ->
      fprintf ppf "`%s" tag
  | (`Closure _ | `Primitive _) as e -> pp_fun ppf e

let pp_value ppf = pp_value_aux pp_fun ppf

let pp_value_nofun ppf = pp_value_aux (fun ppf _ -> fprintf ppf "<fun>") ppf
