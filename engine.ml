(* © 2016-2022 Tommaso Petrucciani, see the README.md and LICENSE.md files *)
open Utils
open Syntax

module CD = Cduce_lib

let fprintf = Format.fprintf

let do_run = ref true
let do_type = ref true
let do_run_unsafe = ref false
let do_echo = ref false
let do_echo_desugared = ref false
let do_echo_debruijn = ref false
let do_show_fun = ref false
let do_print_type = ref true
let do_print_constraints = ref false

type full_env = {
  naming: name Env.t;
  values: value Env.t;
  typing: Types.type_env
}

let starting_env = Primitives.({ naming; values; typing })

let string_of_why_stuck = function
  | `Unbound -> "unbound variable"
  | `Not_a_function -> "not a function"
  | `No_match -> "match failure"
  | `Primitive_fail err -> "primitive failure (" ^ err ^ ")"

let pp_binding ppf = function
    | Some p -> fprintf ppf "let %a = " Print.pp_pattern p
    | None -> ()

let process_binding ppf pp_value env p v =
  match Semantics.eval_match v p with
  | Some env' ->
      let naming' = pattern_naming p in
      List.iter2 (fun x v ->
          fprintf ppf "val %s = @[%a@]@\n%!" x pp_value v)
        (naming' |> Env.to_list |> List.rev) (env' |> Env.to_list |> List.rev);
      (Env.update_many env.naming naming', Env.update_many env.values env')
  | None ->
      fprintf ppf "Error: match failure.@\n%!";
      (env.naming, env.values)

let process_expr ppf env ?binding e =
  let e' = desugar e in
  let e'' = de_bruijn env.naming e' in
  if !do_echo
  then fprintf ppf "<ECHO>    %a@[%a@]@\n%!"
    pp_binding binding Print.pp_ext_expr e;
  if !do_echo_desugared
  then fprintf ppf "<ECHO-ds> %a@[%a@]@\n%!"
    pp_binding binding Print.pp_expr e';
  if !do_echo_debruijn
  then fprintf ppf "<ECHO-db> %a@[%a@]@\n%!"
    pp_binding binding Print.pp_db_expr e'';
  let pp_value = if !do_show_fun then Print.pp_value else Print.pp_value_nofun
  in
  let (iswelltyped, typing) =
    if !do_type then
      begin
        let alpha = Types.fresh_var_type () in
        let (penv, pcs) =
          match binding with
          | Some p -> Constr.generate_pattern alpha p
          | None -> (Types.Type_env.singleton "-" alpha, [])
        in
        let cs = Constr.generate alpha e' @ pcs in
        if !do_print_constraints
        then fprintf ppf "<CONSTRS>@\n%a@\n@\n%!" Constr.pp_constrset cs;
        match Constr.solve env.typing cs |> Constr.Nondet.to_list with
        | [] -> fprintf ppf "Type error.@\n%!"; (false, env.typing)
        | thetas ->
            let penv' =
              Types.Type_env.map (fun t -> Types.intersect_and_clean t thetas)
                penv
            in
            Types.Type_env.iter (fun x t ->
                if !do_print_type
                then fprintf ppf "%s : %a@\n%!" x Types.pp_type t)
              penv';
            match binding with
            | Some _ ->
                let typing = Types.Type_env.map Types.poly_scheme penv'
                  |> Types.Type_env.update env.typing
                in (true, typing)
            | None -> (true, env.typing)
      end
    else (true, env.typing)
  in
  let (naming, values) =
    if !do_run && (iswelltyped || !do_run_unsafe) then
      begin
        match Semantics.eval env.values e'' with
        | `Stuck why ->
            fprintf ppf "Error: %s.@\n%!" (string_of_why_stuck why);
            (env.naming, env.values)
        | `Value v ->
            begin match binding with
            | Some p when p <> PWcard -> process_binding ppf pp_value env p v
            | _ ->
                fprintf ppf "- = @[%a@]@\n%!" pp_value v;
                (env.naming, env.values)
            end
      end
    else (env.naming, env.values)
  in
  { naming; values; typing }

let process ppf env = function
  | Binding (p, e) -> Some (process_expr ppf env ~binding:p e)
  | Expr e -> Some (process_expr ppf env e)
  | Quit -> None
