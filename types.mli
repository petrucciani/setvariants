(* © 2016-2022 Tommaso Petrucciani, see the README.md and LICENSE.md files *)
open Utils

module CD = Cduce_types

(* Type definitions from CDuce. *)
type t = CD.Types.t
type var = CD.Var.t
type varset = CD.Var.Set.t
type subst = CD.Types.Subst.t

(* Fresh type variable generators. *)
val fresh_var: unit -> var
val fresh_var_type: unit -> t

val apply_subst: subst -> t -> t
val mvar: t -> varset

(* Type schemes. *)
type scheme

val scheme: t -> varset -> scheme
val mono_scheme: t -> scheme
(* Quantifies all variables. *)
val poly_scheme: t -> scheme
val gen_delta: varset -> t -> scheme
val apply_subst_scheme: subst -> scheme -> scheme
(* Instantiates the scheme with fresh variables. *)
val instance: scheme -> t
val mvar_scheme: scheme -> varset

(* Type environments. *)
module Type_env: sig
  include Map.S with type key = name
  (* find_opt x env = Some t if (x: t) in env, None otherwise. *)
  val find_opt: name -> 'a t -> 'a option
  (* update env1 env2 updates env1 with the bindings in env2. *)
  val update: 'a t -> 'a t -> 'a t
end

type type_env = scheme Type_env.t
type mono_env = t Type_env.t

val all_mvars: type_env -> varset

val gen: type_env -> t -> scheme
val gen_delta_mono_env: varset -> mono_env -> type_env
val gen_mono_env: type_env -> mono_env -> type_env
val apply_subst_env: subst -> type_env -> type_env
val apply_subst_mono_env: subst -> mono_env -> mono_env

val intersect_and_clean: t -> subst list -> t

(* Pretty-printing. *)
val pp_type: Format.formatter -> t -> unit
val pp_scheme: Format.formatter -> scheme -> unit
val pp_env: Format.formatter -> type_env -> unit
val pp_mono_env: Format.formatter -> mono_env -> unit
