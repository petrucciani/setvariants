var worker = new Worker ("web.js");
var worker_handler = new Object ();

worker.onmessage = function (m) {
    if (typeof m.data == 'string') {
        console.log("" + m.data);
    } else {
        console.log ("[ASYNCH] back from " + m.data.fname);
        var handler = worker_handler[m.data.fname];
        handler (m.data.result);
    }
}

function ASYNCH (action_name, action_args, cont) {
    worker_handler[action_name] = cont;
    worker.postMessage ({fname: action_name, args: action_args});
    console.log ("[ASYNCH] " + action_name + " (" + action_args + ")");
}

function endswith(str, target) {
   return str.substring(str.length - target.length) == target;
}

var entityMap = {
  "&": "&amp;",
  "<": "&lt;",
  ">": "&gt;",
  '"': '&quot;',
  "'": '&#39;',
  "/": '&#x2F;'
};

function escapeHtml(string) {
  return String(string).replace(/[&<>"'\/]/g, function (s) {
    return entityMap[s];
  });
}

function process(s) {
	var s1 = $.trim(s);
	var s2 = endswith(s1, ";;") ? s1 : s1 + ";;";
	$("#input-text").val("");
  $("#output").html($("#output").html() + "# " + escapeHtml(s2) + "\n");
  $("#prompt").html("");
	ASYNCH ("process", [s2],
		function (output) {
			$("#output").html($("#output").html() + escapeHtml(output));
      $("#prompt").html("# ");
		});
}

$(document).ready(
	function () {
		$("#input-text").keypress(
			function(event) {
				if (event.which == 13 && !event.shiftKey) {
					process($("#input-text").val());
					event.preventDefault();
				}
			});
    $(".copypaste").click(
      function(event) {
        var s = $(this).find("pre:first-of-type").text();
        process(s);
      });
	});
