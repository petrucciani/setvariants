(* © 2016-2022 Tommaso Petrucciani, see the README.md and LICENSE.md files *)
(* Boilerplate code for calling OCaml in the worker thread. *)
let js_object = Js.Unsafe.variable "Object"
let js_handler = jsnew js_object ()
let postMessage = Js.Unsafe.variable "postMessage"

let log s = ignore (Js.Unsafe.call postMessage (Js.Unsafe.variable "self")
          [|Js.Unsafe.inject (Js.string s)|])

let onmessage event =
  let fname = event##data##fname in
  let args = event##data##args in
  let handle = Js.Unsafe.get js_handler fname in
  let result = Js.Unsafe.fun_call handle (Js.to_array args) in
  let response = jsnew js_object () in
  Js.Unsafe.set response (Js.string "fname") fname;
  Js.Unsafe.set response (Js.string "result") result;
  Js.Unsafe.call postMessage (Js.Unsafe.variable "self") [|Js.Unsafe.inject response|]

let _ = Js.Unsafe.set (Js.Unsafe.variable "self") (Js.string "onmessage") onmessage

(* * * * *)

let fprintf = Format.fprintf
let str_formatter = Format.str_formatter
let flush = Format.flush_str_formatter

let parse_statement s = Parser.statement Lexer.token (Lexing.from_string s)

let env = ref Engine.starting_env

let process s =
  try
    let stmt = parse_statement s in
    match Engine.process str_formatter !env stmt with
    | Some env' -> env := env'; flush ()
    | None -> (); ""
  with Parser.Error -> fprintf str_formatter "Parser error.@\n%!"; flush ()
     | Lexer.Error s -> fprintf str_formatter "Lexer error: %s@\n%!" s; flush ()
     | Utils.Lookup_exn s -> fprintf str_formatter "Error: %s@\n%!" s; flush ()

let run s = Js.string (process (Js.to_string s))

let _ =
  (* Primitives.register_globals (); *)
  Js.Unsafe.set js_handler (Js.string "process") (Js.wrap_callback run)
