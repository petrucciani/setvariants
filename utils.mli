(* © 2016-2022 Tommaso Petrucciani, see the README.md and LICENSE.md files *)
(* Expression variables. *)
type name = string

(* Variant tags/labels. *)
type label = string

module IntSet: Set.S with type elt = int
module StringSet: Set.S with type elt = string
module StringMap: Map.S with type key = string

exception Lookup_exn of string

(* Environments. *)
module Env: sig
  type 'a t

  val empty: 'a t
  val singleton: 'a -> 'a t
  val of_list: 'a list -> 'a t
  val to_list: 'a t -> 'a list

  val is_empty: 'a t -> bool
  val lookup: 'a t -> int -> 'a
  val update: 'a t -> 'a -> 'a t
  (* Elements are added from the last. *)
  val update_many: 'a t -> 'a t -> 'a t

  val map: ('a -> 'b) -> 'a t -> 'b t
  val iteri: (int -> 'a -> unit) -> 'a t -> unit

  val index_of: name t -> name -> int
  val subenv: IntSet.t -> 'a t -> 'a t * (int -> int)
end

type naming = name Env.t
