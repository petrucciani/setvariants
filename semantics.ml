(* © 2016-2022 Tommaso Petrucciani, see the README.md and LICENSE.md files *)
open Utils
open Syntax

exception Match_fail

let reshuffle p1 p2 env2 =
  let naming1 = pattern_naming p1 in
  let naming2 = pattern_naming p2 in
  Env.map (fun x ->
      let i = Env.index_of naming2 x in Env.lookup env2 i)
    naming1

let eval_match v p =
  let rec aux v = function
    | PWcard -> Env.empty
    | PVar _ -> Env.singleton v
    | PConst c -> if v = `Const c then Env.empty else raise Match_fail
    | PFun ->
        begin match v with
        | `Closure _ | `Primitive _ -> Env.empty
        | _ -> raise Match_fail
        end
    | PPair (p1, p2) ->
        begin match v with
        | `Pair (v1, v2) -> Env.update_many (aux v1 p1) (aux v2 p2)
        | _ -> raise Match_fail
        end
    | PVariant (tag, p1) ->
        begin match v with
        | `Variant (tag', v1) when tag = tag' -> aux v1 p1
        | _ -> raise Match_fail
        end
    | PVariant' tag -> if v = `Variant' tag then Env.empty else raise Match_fail
    | PAnd (p1, p2) -> Env.update_many (aux v p1) (aux v p2)
    | POr (p1, p2) ->
        try aux v p1
        with Match_fail -> reshuffle p1 p2 (aux v p2)
  in
  try Some (aux v p)
  with Match_fail -> None

type why_stuck =
  [ `Unbound
  | `Not_a_function
  | `No_match
  | `Primitive_fail of string
  ]

exception Stuck of why_stuck

let get_stuck why = raise (Stuck why)

let rec get_outer_vars n = function
  | `Var n' -> if n' >= n then IntSet.singleton (n' - n) else IntSet.empty
  | `Const _ -> IntSet.empty
  | `Abstr (f, x, e) ->
      get_outer_vars (match f with Some _ -> n + 2 | None -> n + 1) e
  | `Appl (e1, e2) -> IntSet.union (get_outer_vars n e1) (get_outer_vars n e2)
  | `Pair (e1, e2) -> IntSet.union (get_outer_vars n e1) (get_outer_vars n e2)
  | `Variant (_, e) -> get_outer_vars n e
  | `Variant' _ -> IntSet.empty
  | `Match (e0, clauses) ->
      List.fold_left (fun vars (pi, ei) ->
          get_outer_vars (n + how_many_capt pi) ei |> IntSet.union vars)
        (get_outer_vars n e0) clauses

let rec remap_vars remap n = function
  | `Var n' as e -> if n' >= n then `Var (remap (n' - n) + n) else e
  | `Const _ as e -> e
  | `Abstr (f, x, e) ->
      `Abstr (f, x,
        remap_vars remap (match f with Some _ -> n + 2 | None -> n + 1) e)
  | `Appl (e1, e2) -> `Appl (remap_vars remap n e1, remap_vars remap n e2)
  | `Pair (e1, e2) -> `Pair (remap_vars remap n e1, remap_vars remap n e2)
  | `Variant (tag, e) -> `Variant (tag, remap_vars remap n e)
  | `Variant' _ as e -> e
  | `Match (e0, clauses) ->
      `Match (remap_vars remap n e0,
        List.map (fun (pi, ei) ->
          (pi, remap_vars remap (n + how_many_capt pi) ei)) clauses)

let eval env e =
  let rec aux env = function
    | `Var n -> (try Env.lookup env n with _ -> get_stuck `Unbound)
    | `Const c -> `Const c
    | `Abstr (f, x, e) ->
        let n = match f with Some _ -> 2 | _ -> 1 in
        let (env', remap) = Env.subenv (get_outer_vars n e) env in
        `Closure (f, x, remap_vars remap n e, env')
    | `Appl (e1, e2) ->
        begin match aux env e1 with
          | `Closure (f, x, body, env') as v1 ->
              let v2 = aux env e2 in
              let env'' =
                match f with Some _ -> Env.update env' v1 | None -> env'
              in
              aux (Env.update env'' v2) body
          | `Primitive (x, op) ->
              (try op (aux env e2)
              with Primitive_error err -> get_stuck (`Primitive_fail err))
          | _ -> get_stuck `Not_a_function
        end
    | `Pair (e1, e2) -> `Pair (aux env e1, aux env e2)
    | `Variant (tag, e) -> `Variant (tag, aux env e)
    | `Variant' tag -> `Variant' tag
    | `Match (e0, clauses) ->
        let rec do_matching v = function
          | (pi, ei) :: clauses' ->
              begin match eval_match v pi with
              | Some env' -> aux (Env.update_many env env') ei
              | None -> do_matching v clauses'
              end
          | [] -> get_stuck `No_match
        in
        do_matching (aux env e0) clauses
  in
  try `Value (aux env e)
  with Stuck why -> `Stuck why
