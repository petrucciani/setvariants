(* © 2016-2022 Tommaso Petrucciani, see the README.md and LICENSE.md files *)
type name = string
type label = string

module IntSet = Set.Make(
  struct type t = int let compare = compare end)
module StringSet = Set.Make(
  struct type t = string let compare = String.compare end)
module StringMap = Map.Make(
  struct type t = string let compare = String.compare end)

exception Lookup_exn of string

module Env = struct
  type 'a t = 'a Vec.t

  let empty = Vec.empty
  let singleton = Vec.singleton
  let of_list = Vec.of_list
  let to_list = Vec.to_list

  let fail_lookup err = raise (Lookup_exn err)

  let is_empty = Vec.is_empty
  let lookup env n =
    try Vec.get n env
    with Vec.Vec_index_out_of_bounds -> fail_lookup (string_of_int n)
  let update env x = Vec.insert 0 x env
  let update_many env env' = Vec.concat env' env

  let map = Vec.map
  let iteri = Vec.iteri

  exception Found of int

  let findi env x =
    try
      Vec.iteri (fun i y -> if x = y then raise (Found i)) env;
      raise Not_found
    with Found i -> i

  let index_of naming x =
    try findi naming x
    with Not_found -> fail_lookup ("Identifier " ^ x ^ " is unbound.")

  let subenv is env =
    let is' = IntSet.fold Vec.append is Vec.empty in
    let env' = Vec.map (fun i -> Vec.get i env) is' in
    let remap i = findi is' i in
    (env', remap)
end

type naming = name Env.t
