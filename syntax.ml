(* © 2016-2022 Tommaso Petrucciani, see the README.md and LICENSE.md files *)
open Utils

type constant =
  | CUnit
  | CBool of bool
  | CInt of int

type pattern =
  | PWcard
  | PVar of name
  | PConst of constant
  | PFun
  | PPair of pattern * pattern
  | PVariant of label * pattern
  | PVariant' of label
  | PAnd of pattern * pattern
  | POr of pattern * pattern

type 'var_type int_expr =
  [ `Var of 'var_type
  | `Const of constant
  | `Abstr of name option * name * 'var_type int_expr
  | `Appl of 'var_type int_expr * 'var_type int_expr
  | `Pair of 'var_type int_expr * 'var_type int_expr
  | `Variant of label * 'var_type int_expr
  | `Variant' of label
  | `Match of 'var_type int_expr * (pattern * 'var_type int_expr) list
  ]

type expr = name int_expr
type db_expr = int int_expr

exception Primitive_error of string
type primitive_op = value -> value

and value =
  [ `Const of constant
  | `Closure of name option * name * db_expr * closure_env
  | `Pair of value * value
  | `Variant of label * value
  | `Variant' of label
  | `Primitive of name * primitive_op
  ]

and closure_env = value Env.t

type ext_expr =
  [ `Var of name
  | `Const of constant
  | `Abstr of name option * pattern list * ext_expr
  | `Abstr_cases of name option * (pattern * ext_expr) list
  | `Appl of ext_expr * ext_expr
  | `Pair of ext_expr * ext_expr
  | `Variant of label * ext_expr
  | `Variant' of label
  | `Match of ext_expr * (pattern * ext_expr) list
  | `Let of pattern * ext_expr * ext_expr
  | `If_then_else of ext_expr * ext_expr * ext_expr
  | `Empty_list
  | `Cons of ext_expr * ext_expr
  | `List of  ext_expr list
  | `Let_abstr of name * bool * pattern list * ext_expr * ext_expr
  ]

let rec capt: pattern -> StringSet.t =
  let open StringSet in
  function
  | PWcard -> empty
  | PVar x -> singleton x
  | PConst c -> empty
  | PFun -> empty
  | PPair (p1, p2) -> union (capt p1) (capt p2)
  | PVariant (tag, p) -> capt p
  | PVariant' tag -> empty
  | PAnd (p1, p2) -> union (capt p1) (capt p2)
  | POr (p1, _) -> capt p1

let rec how_many_capt = function
  | PWcard -> 0
  | PVar _ -> 1
  | PConst _ -> 0
  | PFun -> 0
  | PPair (p1, p2) -> how_many_capt p1 + how_many_capt p2
  | PVariant (_, p) -> how_many_capt p
  | PVariant' _ -> 0
  | PAnd (p1, p2) -> how_many_capt p1 + how_many_capt p2
  | POr (p1, _) -> how_many_capt p1

let rec all_names =
  let open StringSet in
  function
  | `Var n -> singleton n
  | `Const c -> empty
  | `Appl (e1, e2) -> union (all_names e1) (all_names e2)
  | `Pair (e1, e2) -> union (all_names e1) (all_names e2)
  | `Variant (tag, e) -> all_names e
  | `Variant' tag -> empty
  | `Match (e, clauses) ->
      List.fold_left (fun names (p, e) ->
          union (capt p) (all_names e) |> union names)
        (all_names e)
        clauses
  | `Abstr (f, ps, e) ->
      List.fold_left (fun names p -> union names (capt p))
        (match f with Some f' -> singleton f' | None -> empty) ps
      |> union (all_names e)
  | `Abstr_cases (f, clauses) ->
      List.fold_left (fun names (p, e) ->
          union (capt p) (all_names e) |> union names)
        (match f with Some f' -> singleton f' | None -> empty)
        clauses
  | `Let (p, e1, e2) ->
      union (capt p) (union (all_names e1) (all_names e2))
  | `If_then_else (e1, e2, e3) ->
      union (all_names e1) (union (all_names e2) (all_names e3))
  | `Empty_list -> empty
  | `Cons (e1, e2) -> union (all_names e1) (all_names e2)
  | `List es ->
      List.fold_left (fun names e -> all_names e |> union names) empty es
  | `Let_abstr (f, isrec, ps, e1, e2) ->
      List.fold_left (fun names p -> union names (capt p))
        (singleton f) ps
      |> union (all_names e1) |> union (all_names e2)

let rec build_list = function
  | e :: es -> `Variant ("Cons", `Pair (e, build_list es))
  | [] -> `Variant' "Nil"

let desugar: ext_expr -> expr =
  fun e ->
    let names = all_names e in
    let counter = ref 0 in
    let rec fresh_name () =
      incr counter;
      let x = "_x" ^ (string_of_int !counter) in
      if StringSet.mem x names then fresh_name () else x
    in
    let rec aux: ext_expr -> expr = function
      | (`Var _ | `Const _ | `Variant' _) as e -> e
      | `Appl (e1, e2) -> `Appl (aux e1, aux e2)
      | `Pair (e1, e2) -> `Pair (aux e1, aux e2)
      | `Variant (tag, e) -> `Variant (tag, aux e)
      | `Match (e, clauses) ->
          `Match (aux e, List.map (fun (pi, ei) -> (pi, aux ei)) clauses)
      | `Abstr (f, ps, e) ->
          begin match ps with
          | PVar x :: ((_ :: _) as ps') ->
              let e' = `Abstr (None, ps', e) in
              `Abstr (f, x, aux e')
          | p :: ((_ :: _) as ps') ->
              let x = fresh_name () in
              let e' = `Abstr (None, ps', e) in
              `Abstr (f, x, `Match (`Var x, [p, aux e']))
          | [PVar x] ->
              `Abstr (f, x, aux e)
          | [p] ->
              let x = fresh_name () in
              `Abstr (f, x, `Match (`Var x, [(p, aux e)]))
          | [] -> assert false
          end
      | `Abstr_cases (f, clauses) ->
          let x = fresh_name () in
          let clauses' = List.map (fun (pi, ei) -> (pi, aux ei)) clauses in
          `Abstr (f, x, `Match (`Var x, clauses'))
      | `Let (p, e1, e2) -> `Match (aux e1, [(p, aux e2)])
      | `If_then_else (e1, e2, e3) ->
          `Match (aux e1,
            [(PConst (CBool true), aux e2); (PConst (CBool false), aux e3)])
      | `Empty_list -> `Variant' "Nil"
      | `Cons (e1, e2) -> `Variant ("Cons", `Pair (aux e1, aux e2))
      | `List es -> build_list (List.map aux es)
      | `Let_abstr (f, isrec, ps, e1, e2) ->
          let abstr = `Abstr ((if isrec then Some f else None), ps, e1) in
          `Match (aux abstr, [(PVar f, aux e2)])
    in
    aux e

let rec pattern_naming p =
  let open StringSet in
  match p with
  | PWcard -> Env.empty
  | PVar x -> Env.singleton x
  | PConst c -> Env.empty
  | PFun -> Env.empty
  | PPair (p1, p2) -> Env.update_many (pattern_naming p1) (pattern_naming p2)
  | PVariant (tag, p) -> pattern_naming p
  | PVariant' tag -> Env.empty
  | PAnd (p1, p2) -> Env.update_many (pattern_naming p1) (pattern_naming p2)
  | POr (p1, _) -> pattern_naming p1

let rec de_bruijn: naming -> expr -> db_expr =
  fun naming -> function
  | `Var x -> `Var (Env.index_of naming x)
  | `Const _ as e -> e
  | `Abstr (f, x, e) ->
      let naming' = Env.(
        update (match f with Some f' -> update naming f' | None -> naming) x)
      in
      `Abstr (f, x, de_bruijn naming' e)
  | `Appl (e1, e2) -> `Appl (de_bruijn naming e1, de_bruijn naming e2)
  | `Pair (e1, e2) -> `Pair (de_bruijn naming e1, de_bruijn naming e2)
  | `Variant (tag, e) -> `Variant (tag, de_bruijn naming e)
  | `Variant' tag -> `Variant' tag
  | `Match (e, clauses) ->
      let clauses' = List.map (fun (pi, ei) ->
          (pi, de_bruijn (pattern_naming pi |> Env.update_many naming) ei))
        clauses
      in
      `Match (de_bruijn naming e, clauses')

type statement =
  | Binding of pattern * ext_expr
  | Expr of ext_expr
  | Quit
