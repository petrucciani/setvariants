(* © 2016-2022 Tommaso Petrucciani, see the README.md and LICENSE.md files *)
open Utils
open Syntax

val pp_constant: Format.formatter -> constant -> unit
val pp_pattern: Format.formatter -> pattern  -> unit

val pp_expr: Format.formatter -> expr -> unit
val pp_db_expr: Format.formatter -> db_expr -> unit

val pp_ext_expr: Format.formatter -> ext_expr -> unit

val pp_value: Format.formatter -> value -> unit
(* Prints <fun> on functional values. *)
val pp_value_nofun: Format.formatter -> value -> unit
