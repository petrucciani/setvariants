(* © 2016-2022 Tommaso Petrucciani, see the README.md and LICENSE.md files *)
open Utils
open Syntax

type why_stuck =
  [ `Unbound
  | `Not_a_function
  | `No_match
  | `Primitive_fail of string
  ]

val eval_match: value -> pattern -> value Env.t option

val eval: value Env.t -> db_expr -> [ `Value of value | `Stuck of why_stuck ]
