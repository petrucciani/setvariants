(* © 2016-2022 Tommaso Petrucciani, see the README.md and LICENSE.md files *)
open Utils
open Syntax

(* Primitives:
   - eq: Any -> Any -> bool
     checks for equality (runtime error on functions)
   - succ: int -> int
   - add: int -> int -> int
   - sub: int -> int -> int
   - mul: int -> int -> int
   - div: int -> int -> int
   - safe_div: int -> (int \ 0) -> int
 *)

val naming: name Env.t

val values: value Env.t

val typing: Types.type_env

val register_globals: unit -> unit
