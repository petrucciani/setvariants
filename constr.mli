(* © 2016-2022 Tommaso Petrucciani, see the README.md and LICENSE.md files *)
open Utils

val do_print_tallying_constraints: bool ref

(* Constraint representation. *)

type constr =
  | CSub of Types.t * Types.t
  | CInst of name * Types.t
  | CDef of Types.mono_env * constrset
  | CLet of constrset * ((Types.mono_env * constr list) list)

and constrset = constr list

type typeconstr = Types.t * Types.t

(* Constraint generation. *)

val generate: Types.t -> Syntax.expr -> constrset

val generate_pattern: Types.t -> Syntax.pattern -> Types.mono_env * constrset

(* Handling non-determinism. *)

module Nondet: sig
  type 'a t

  val return: 'a -> 'a t
  val bind: 'a t -> ('a -> 'b t) -> 'b t
  val ( >>= ): 'a t -> ('a -> 'b t) -> 'b t

  val flatten: 'a t list -> 'a list t

  val of_list: 'a list -> 'a t
  val to_list: 'a t -> 'a list
end

(* Yields all possible unions. *)
val nondet_union:
  (typeconstr list * Types.subst list) Nondet.t ->
  (typeconstr list * Types.subst list) Nondet.t ->
  (typeconstr list * Types.subst list) Nondet.t

val nondet_union_many:
  (typeconstr list * Types.subst list) Nondet.t list ->
  (typeconstr list * Types.subst list) Nondet.t

(* Constraint rewriting and solving. *)

val rewrite:
  Types.type_env -> constrset -> (typeconstr list * Types.subst list) Nondet.t

val tally: typeconstr list -> Types.subst list -> Types.subst Nondet.t

val solve: Types.type_env -> constrset -> Types.subst Nondet.t

(* Pretty printing. *)

val pp_constrset: Format.formatter -> constrset -> unit
