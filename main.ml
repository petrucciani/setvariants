(* © 2016-2022 Tommaso Petrucciani, see the README.md and LICENSE.md files *)
open Utils
open Syntax

module CD = Cduce_lib

let spec =
	Arg.(Engine.(align [
		("--no-run", Clear do_run, " Do not evaluate");
		("--no-type", Clear do_type, " Do not perform type reconstruction");
    ("--run-unsafe", Set do_run_unsafe, " Run also if ill-typed");
		("--echo", Set do_echo, " Print parsed expressions");
		("--echo-desugared", Set do_echo_desugared, " Print desugared expressions");
    ("--echo-de-bruijn", Set do_echo_debruijn,
      " Print after conversion to De Bruijn notation");
    ("--echo-all", Unit (fun () ->
        do_echo := true;
        do_echo_desugared := true;
        do_echo_debruijn := true),
      " Like --echo, --echo-desugared, and --echo-de-bruijn");
    ("--show-fun", Set do_show_fun, " Print definition of functional values");
    ("--show-constraints", Set do_print_constraints,
      " Print generated constraints");
    ("--show-tallying-constraints", Set Constr.do_print_tallying_constraints,
      " Print constraints when callying tallying")
	]))

let usage_msg =
	"Prototype of the set-theoretic type system for polymorphic variants."

let () =
	Arg.parse spec (fun s ->
			raise (Arg.Bad ("invalid argument '" ^ s ^ "'")))
		usage_msg;
  (* Primitives.register_globals (); *)
	let inchannel = stdin in
	let lexbuf = Lexing.from_channel inchannel in
	let read_statement () = Parser.statement Lexer.token lexbuf in
	let rec loop env =
    Format.printf "# %!";
    try
      let statement = read_statement () in
      match Engine.process Format.std_formatter env statement with
      | Some env' -> loop env'
      | None -> ()
    with Parser.Error -> Format.printf "Parser error.@\n%!"; loop env
       | Lexer.Error s -> Format.printf "Lexer error: %s@\n%!" s; loop env
       | Utils.Lookup_exn s -> Format.printf "Error: %s@\n%!" s; loop env
  in
  loop Engine.starting_env


(*
'a_115_Source \ `A((`dummy \ 'a103_116_Source)) -> 'a_115_Source \ `A((`dummy \ 'a103_116_Source)) <:
'a105_0_Source -> 'a104_0_Source -> 'a98_0_Source

'a_117_Source \ `A((`dummy \ 'a103_118_Source)) -> 'a_117_Source \ `A((`dummy \ 'a103_118_Source)) <:
'a105_0_Source

'a_119_Source \ `A((`dummy \ 'a103_120_Source)) -> 'a_119_Source \ `A((`dummy \ 'a103_120_Source)) <:
'a106_0_Source -> 'a104_0_Source

`A('a107_0_Source) <: 'a106_0_Source

`dummy <: 'a107_0_Source

---

'a \ `A((`dummy \ 'a103)) -> 'a \ `A((`dummy \ 'a103)) <: 'a105 -> 'a104 -> 'a98
'a \ `A((`dummy \ 'a103)) -> 'a \ `A((`dummy \ 'a103)) <: 'a105
'a \ `A((`dummy \ 'a103)) -> 'a \ `A((`dummy \ 'a103)) <: 'a106 -> 'a104
`A('a107) <: 'a106
`dummy <: 'a107

'a \ `A((`dummy \ 'b)) -> 'a \ `A((`dummy \ 'b)) <: 'c -> 'd -> 'e
'a \ `A((`dummy \ 'b)) -> 'a \ `A((`dummy \ 'b)) <: 'c
'a \ `A((`dummy \ 'b)) -> 'a \ `A((`dummy \ 'b)) <: 'f -> 'd

'a \ (`A, `B \ 'b) -> 'a \ (`A, `B \ 'b) <: 'c -> 'd -> 'e
'a \ (`A, `B \ 'b) -> 'a \ (`A, `B \ 'b) <: 'c
'a \ (`A, `B \ 'b) -> 'a \ (`A, `B \ 'b) <: 'f -> 'd
*)

(*
(`A(`dummy),`B(`dummy)) | (`B(`dummy),`A(`dummy)) -> Bool <: 'a0147 -> 'a0146
('a0149,'a0148) <: 'a0147
`A('a0150) <: 'a0149
`dummy <: 'a0150
`A('a0151) <: 'a0148
`dummy <: 'a0151

(`A(`dummy),`B(`dummy)) | (`B(`dummy),`A(`dummy)) -> Bool <: 'c -> 'd
('a,'b) <: 'c
`A(`dummy) <: 'a
`A(`dummy) <: 'b
*)
(*
let () =
	let open CD in
	let _maken n = n |> Intervals.V.from_int |> Intervals.atom
                |> Types.interval
  in
  let mk_atom s = s |> CD.Atoms.V.mk_ascii |> CD.Atoms.atom |> CD.Types.atom
	in
  let makevar name = Var.mk ~internal:false name |> Types.var
	in
	let makevar' name id k = Var.mkmk name id k |> Types.var
	in
  let arrow t t' = Types.arrow (Types.cons t) (Types.cons t') in
  let times t t' = Types.times (Types.cons t) (Types.cons t') in
  let variant tag t =
    CD.Types.xml
     (tag |> mk_atom |> CD.Types.cons)
     (CD.Types.cons
       (CD.Types.times
         (CD.Types.cons (CD.Types.empty_closed_record))
         (t |> CD.Types.cons)))
  in
  let a = makevar "a0149" in
  let b = makevar "a0148" in
  let c = makevar "a0147" in
  let d = makevar "a0146" in
  let e = makevar "a0150" in
  let f = makevar "a0151" in
  let dummy =  (mk_atom "dummy") in
  let adummy = variant "A" e in
  let bdummy = variant "B" f in
  let tbool = CD.Types.cup (mk_atom "true") (mk_atom "false") in
(*(`A(`dummy),`B(`dummy)) | (`B(`dummy),`A(`dummy)) -> Bool <: 'c -> 'd
('a,'b) <: 'c
`A(`dummy) <: 'a
`A(`dummy) <: 'b*)
  let l = [
      ( arrow (CD.Types.cup (times adummy bdummy) (times bdummy adummy))
          tbool,
        arrow c d );
      ( times a b, c );
      (adummy, a);
      (adummy, b);
      (dummy, e);
      (dummy, f)
    ]
  in
  Constr.tally l [] |> Constr.Nondet.to_list |> Format.printf "%a\n\n%!" CD.Type_tallying.pp_sl;
  Format.eprintf "done\n\n%!"
*)
(*
  let c = makevar' "c" 0 `S in
  let d = makevar' "d" 0 `S in
  let e = makevar' "e" 0 `S in
  (* let f = makevar' "f" 0 `S in
  let g = makevar' "g" 0 `S in *)
  let aatom = mk_atom "A" in
  let batom = mk_atom "B" in
  let _empty = Types.empty in
  let arrow t t' = Types.arrow (Types.cons t) (Types.cons t') in
  let times t t' = Types.times (Types.cons t) (Types.cons t') in
  let _variant tag t =
    CD.Types.xml
     (tag |> mk_atom |> CD.Types.cons)
     (CD.Types.cons
       (CD.Types.times
         (CD.Types.cons (CD.Types.empty_closed_record))
         (t |> CD.Types.cons)))
  in
  let tt a b = Types.diff a (times aatom (Types.diff batom b)) in
  let tt1 = tt (makevar' "a1" 1 `S) (makevar' "b1" 0 `S) in
  let tt2 = tt (makevar' "a2" 1 `S) (makevar' "b2" 0 `S) in
  let tt3 = tt (makevar' "a3" 1 `S) (makevar' "b3" 0 `S) in
  let l = [
  		(arrow tt1 tt1, arrow (arrow tt2 tt2) (arrow d e));
  		(* (arrow tt2 tt2, c); *)
  		(arrow tt3 tt3, arrow (times aatom batom) d)(*;
  		(times aatom batom, f);
  		(batom, g) *)
	  ]
	in
	Constr.tally l [] |> ignore;
	Format.eprintf "done\n\n%!"
*)
(*
'a_115_Source \ `A((`dummy \ 'a103_116_Source)) -> 'a_115_Source \ `A((`dummy \ 'a103_116_Source)) <:
'a105_0_Source -> 'a104_0_Source -> 'a98_0_Source

'a_117_Source \ `A((`dummy \ 'a103_118_Source)) -> 'a_117_Source \ `A((`dummy \ 'a103_118_Source)) <:
'a105_0_Source

'a_119_Source \ `A((`dummy \ 'a103_120_Source)) -> 'a_119_Source \ `A((`dummy \ 'a103_120_Source)) <:
'a106_0_Source -> 'a104_0_Source

`A('a107_0_Source) <: 'a106_0_Source

`dummy <: 'a107_0_Source
*)
