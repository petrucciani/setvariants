(* © 2016-2022 Tommaso Petrucciani, see the README.md and LICENSE.md files *)
open Utils
open Syntax

val do_run: bool ref
val do_type: bool ref
val do_run_unsafe: bool ref
val do_echo: bool ref
val do_echo_desugared: bool ref
val do_echo_debruijn: bool ref
val do_show_fun: bool ref
val do_print_type: bool ref
val do_print_constraints: bool ref

type full_env = {
  naming: name Env.t;
  values: value Env.t;
  typing: Types.type_env
}

val starting_env: full_env

val process: Format.formatter -> full_env -> statement -> full_env option
