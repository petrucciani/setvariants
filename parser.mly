/* © 2016-2022 Tommaso Petrucciani, see the README.md and LICENSE.md files */
%{

	open Utils
	open Syntax

	let disjoint capt1 capt2 =
		StringSet.is_empty (StringSet.inter capt1 capt2)

	let build_ppair (p1, capt1) (p2, capt2) =
		if not (disjoint capt1 capt2)
		then failwith "Repeated variables in pair pattern."
		else (PPair (p1, p2), StringSet.union capt1 capt2)

	let rec build_plist = function
		| p :: ps ->
				let p' = build_plist ps in
				let (p'', capt) = build_ppair p p' in
				(PVariant ("Cons", p''), capt)
		| [] -> (PVariant' "Nil", StringSet.empty)

%}

/* Token declarations. */

%token DOT COMMA LAMBDA PAROPEN PARCLOSE BACKQUOTE ARROW PIPE AMPER UNDERSC
%token SEMICOL BRACKOPEN BRACKCLOSE CONS EQUAL DOUBLESEMICOL

%token MATCH WITH FUN REC RECFUN LET IN IF THEN ELSE AS
%token FUNCTION RECFUNCTION

%token <string> IDENT
%token <bool> BOOLEAN
%token <int> INTEGER

%token EOF

%left below_PIPE

%left PIPE
%left AMPER AS

%right    CONS
%nonassoc PAROPEN
%left     INTEGER IDENT BRACKOPEN BOOLEAN
%nonassoc BACKQUOTE

/* Starting production. */

%start prog
%start statement
%start expr

%type < Syntax.statement list >              prog
%type < Syntax.statement >                   statement
%type < Syntax.statement >                   binding
%type < Syntax.ext_expr >                    expr
%type < Syntax.pattern * Syntax.ext_expr >   clause
%type < Syntax.pattern >                     pattern
%type < Syntax.pattern >                     a_pattern
%type < Syntax.pattern * Utils.StringSet.t > pattern_aux

%%

/* Parser definition. */

prog:
	| ss=separated_nonempty_list(DOUBLESEMICOL, non_quit_statement) EOF
			{ ss }

statement:
	| s=non_quit_statement DOUBLESEMICOL
			{ s }
	| EOF
			{ Quit }

non_quit_statement:
	| b=binding
			{ b }
	| e=expr
			{ Expr e }

binding:
	| LET p=pattern EQUAL e=expr
			{ Binding (p, e) }
	| LET f=IDENT ps=a_pattern+ EQUAL e=expr
			{ Binding (PVar f, `Abstr (None, ps, e)) }
	| LET REC f=IDENT ps=a_pattern+ EQUAL e=expr
			{ Binding (PVar f, `Abstr (Some f, ps, e)) }
	| LET REC f=IDENT EQUAL FUNCTION PIPE? cs=clauses
			{ Binding (PVar f, `Abstr_cases (Some f, cs)) }

expr:
	| e=app_expr
			{ e }
	| LAMBDA ps=a_pattern+ DOT   e=expr
	| FUN    ps=a_pattern+ ARROW e=expr
			{ `Abstr (None, ps, e) }
	| RECFUN f=IDENT ps=a_pattern+ ARROW e=expr
			{ `Abstr (Some f, ps, e) }
	| FUNCTION PIPE? cs=clauses
			{ `Abstr_cases (None, cs) }
	| RECFUNCTION f=IDENT PIPE? cs=clauses
			{ `Abstr_cases (Some f, cs) }
	| MATCH e=expr WITH PIPE? cs=clauses
			{ `Match (e, cs) }
	| LET p=pattern EQUAL e1=expr IN e2=expr
			{ `Let (p, e1, e2) }
	| LET f=IDENT ps=a_pattern+ EQUAL e1=expr IN e2=expr
			{ `Let_abstr (f, false, ps, e1, e2) }
	| LET REC f=IDENT ps=a_pattern+ EQUAL e1=expr IN e2=expr
			{ `Let_abstr (f, true, ps, e1, e2) }
	| IF e1=expr THEN e2=expr ELSE e3=expr
			{ `If_then_else (e1, e2, e3) }

app_expr:
	| e=a_expr
			{ e }
	| e1=app_expr e2=a_expr
			{ match e1 with
				| `Variant' tag -> `Variant (tag, e2)
				| e1' -> `Appl (e1', e2) }
	| e1=app_expr CONS e2=app_expr
			{ `Cons (e1, e2) }

a_expr:
	| PAROPEN e1=expr COMMA e2=expr PARCLOSE
			{ `Pair (e1, e2) }
	| PAROPEN e=expr PARCLOSE
			{ e }
	| PAROPEN PARCLOSE
			{ `Const CUnit }
	| BACKQUOTE tag=IDENT
			{ `Variant' tag }
	| BRACKOPEN BRACKCLOSE
			{ `Empty_list }
	| BRACKOPEN es=separated_nonempty_list(SEMICOL, expr) BRACKCLOSE
			{ `List es }
	| b=BOOLEAN
			{ `Const (CBool b) }
	| n=INTEGER
			{ `Const (CInt n) }
	| x=IDENT
			{ `Var x }

clauses:
  | c=clause %prec below_PIPE
	    { [ c ] }
	| c=clause PIPE cs=clauses
	    { c :: cs }

clause:
	| p=pattern ARROW e=expr
			{ (p, e) }

pattern:
	| p=pattern_aux
			{ fst p }

a_pattern:
	| p=a_pattern_aux
			{ fst p }

pattern_aux:
	| p=a_pattern_aux
			{ p }
	| tag=variant_pattern p2=a_pattern_aux
			{ let (p2', capt) = p2 in (PVariant (tag, p2'), capt) }
	| p=pattern_aux AS x=IDENT
			{ let (p', capt) = p in
				if StringSet.mem x capt
				then failwith "Repeated variables in alias pattern."
				else (PAnd (p', PVar x), StringSet.add x capt)
			}
	| p1=pattern_aux AMPER p2=pattern_aux
			{ let (p1', capt1) = p1 in
				let (p2', capt2) = p2 in
				if not (disjoint capt1 capt2)
				then failwith "Repeated variables in & pattern."
				else (PAnd (p1', p2'), StringSet.union capt1 capt2)
			}
	| p1=pattern_aux PIPE p2=pattern_aux
			{ let (p1', capt1) = p1 in
				let (p2', capt2) = p2 in
				if not (StringSet.equal capt1 capt2)
				then failwith "Different variables on the sides of a | pattern."
				else (POr (p1', p2'), capt1)
			}
	| p1=pattern_aux CONS p2=pattern_aux
			{ let (p, capt) = build_ppair p1 p2 in
				(PVariant ("Cons", p), capt) }

a_pattern_aux:
	| PAROPEN p1=pattern_aux COMMA p2=pattern_aux PARCLOSE
			{ build_ppair p1 p2 }
	| PAROPEN p=pattern_aux PARCLOSE
			{ p }
	| PAROPEN PARCLOSE
			{ (PConst CUnit, StringSet.empty) }
	| tag=variant_pattern
			{ (PVariant' tag, StringSet.empty) }
	| BRACKOPEN BRACKCLOSE
			{ (PVariant' "Nil", StringSet.empty) }
	| BRACKOPEN ps=separated_nonempty_list(SEMICOL, pattern_aux) BRACKCLOSE
			{ build_plist ps }
	| UNDERSC
			{ (PWcard, StringSet.empty) }
	| FUN
			{ (PFun, StringSet.empty) }
	| b=BOOLEAN
			{ (PConst (CBool b), StringSet.empty) }
	| n=INTEGER
			{ (PConst (CInt n), StringSet.empty) }
	| x=IDENT
			{ (PVar x, StringSet.singleton x) }

variant_pattern:
	| BACKQUOTE tag=IDENT
			{ tag }
