.PHONY: main main_byte web clean

main:
	ocamlbuild -use-menhir -use-ocamlfind \
	-cflags '-I '`pwd`/../cduce/lib/ \
	-lflags '-I '`pwd`'/../cduce/lib/ 'cduce_lib.cmxa main.native

main_byte:
	ocamlbuild -use-menhir -use-ocamlfind \
	-cflags '-I '`pwd`/../cduce/lib/ \
	-lflags '-I '`pwd`'/../cduce/lib/ 'cduce_lib.cma main.byte

web:
	ocamlbuild -use-menhir -use-ocamlfind \
	-tag-line "not (<**/parser.*>): package(js_of_ocaml), package(js_of_ocaml.syntax), syntax(camlp4o)" \
	-cflags '-I '`pwd`/../cduce/lib/ \
	-lflags '-I '`pwd`'/../cduce/lib/ 'cduce_lib.cma web.byte
	js_of_ocaml +nat.js +dynlink.js +toplevel.js +weak.js web.byte

install: web
	scp base.js jquery.js style.css web.js index.html \
	cduce@dev.cduce.org:/var/www/cduce/ocaml/

clean:
	ocamlbuild -clean
