#Implementation of Set-theoretic types for polymorphic variants

All files are © 2016-2022 Tommaso Pettruciani

tommaso.petrucciani@gmail.com

For any question, you may contact: tommaso.petrucciani@gmail.com,
giuseppe.castagna@irif.fr, kim.nguyen@universite-paris-saclay.fr

This software project is distributed under the terms of the
CeCILL-B license (see file LICENSE.md)
